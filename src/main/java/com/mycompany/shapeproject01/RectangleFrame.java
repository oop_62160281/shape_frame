/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject01;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ASUS
 */
public class RectangleFrame extends JFrame {

    JLabel lblWidth;
    JTextField txtWidth;
    JLabel lblHight;
    JTextField txtHight;
    JButton btnCalculate;
    JLabel lblResult;

    public RectangleFrame() {
        super("Rectangle");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblWidth = new JLabel("Width:", JLabel.TRAILING);
        lblWidth.setSize(50, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        this.add(lblWidth);

        txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 5);
        this.add(txtWidth);

        lblHight = new JLabel("Hight:", JLabel.TRAILING);
        lblHight.setSize(50, 20);
        lblHight.setLocation(5, 20);
        lblHight.setBackground(Color.WHITE);
        lblHight.setOpaque(true);
        this.add(lblHight);

        txtHight = new JTextField();
        txtHight.setSize(50, 20);
        txtHight.setLocation(60, 25);
        this.add(txtHight);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Rectangle width= ??? hight= ??? area= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {//Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidth = txtWidth.getText();
                    double width = Double.parseDouble(strWidth);//-> NumberFormatException
                    String strHight = txtHight.getText();
                    double hight = Double.parseDouble(strHight);
                    Rectangle rectangle = new Rectangle(width, hight);
                    lblResult.setText(" Rectangle width= " + String.format("%.2f", rectangle.getWidth())
                            + " hight =" + String.format("%.2f", rectangle.getHight())
                            + " area = " + String.format("%.2f", rectangle.calArea()));
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtWidth.requestFocus();
                    txtHight.setText("");
                    txtHight.requestFocus();

                }
            }
        });
    }

    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}
