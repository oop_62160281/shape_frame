/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject01;

import javax.swing.JFrame;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ASUS
 */
public class TriangleFrame extends JFrame {

    JLabel lblBase;
    JTextField txtBase;
    JLabel lblHight;
    JTextField txtHight;
    JButton btnCalculate;
    JLabel lblResult;

    public TriangleFrame() {
        super("Triangle");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblBase = new JLabel("Base:", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        this.add(lblBase);

        txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);
        this.add(txtBase);

        lblHight = new JLabel("Hight:", JLabel.TRAILING);
        lblHight.setSize(50, 20);
        lblHight.setLocation(5, 20);
        lblHight.setBackground(Color.WHITE);
        lblHight.setOpaque(true);
        this.add(lblHight);

        txtHight = new JTextField();
        txtHight.setSize(50, 20);
        txtHight.setLocation(60, 25);
        this.add(txtHight);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Triangle base= ??? hight= ??? area= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {//Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText();
                    double base = Double.parseDouble(strBase);//-> NumberFormatException
                    String strHight = txtHight.getText();
                    double hight = Double.parseDouble(strHight);
                    Triangle triangle = new Triangle(base, hight);
                    lblResult.setText(" Triangle base = " + String.format("%.2f", triangle.getBase())
                            + " hight =" + String.format("%.2f", triangle.getHight())
                            + " area = " + String.format("%.2f", triangle.calArea()));
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtBase.requestFocus();
                    txtHight.setText("");
                    txtHight.requestFocus();

                }
            }
        });
    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);

    }
}
