/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject01;

/**
 *
 * @author ASUS
 */
public class Circle extends Shape{
    private double redius;

    public Circle(double redius) {
        super("Circle");
        this.redius = redius;

    }

    public double getRedius() {
        return redius;
    }

    public void setRedius(double redius) {
        this.redius = redius;
    }

    @Override
    public double calArea() {
        return Math.PI * Math.pow(redius, 2);
    }

    @Override
    public double calPerimeter() {
        return 0.5 * Math.PI * redius;
    }
}
